# GitLab-CI Challenge Group3

## Description

This is a simple node js application for educational purpose. 

## Getting started

You can deploy locally this application running the following commands:

```
npm install
npm start 
```

The application is deployed in ```http://localhost:4000```

## Version control strategy

This application use a trunk-based strategy so it has a single permanent branch (the main branch) that is protected and also has temporal feature branches. 
All of the branches trigger a pipeline but only the application is deployed in the main pipeline.

All of the Merge Request must have at least one approval of any member of the team but the author.

## CI/CD Pipeline

The current pipeline have the following stages:

- [x] Security scan
- [x] Build
- [x] Deploy
- [x] Test

### Security stage

It runs in all branches. This stage run a dependency scan (using audit) and static scan (using eslint, nodejs scan and semgrep) over the application. 
The security status of the application can be seen in the Security and Compliance tab.

### Build stage

Install the application using npm and create a zip file with the instalation result. This zip is used as artifact for the deployment.

### Deploy stage

Deploy the application in the configured server using the scripts in the /deploy folder. This script create a trust-base connection with the server, kill all
the npm processes, unzip the artifact and start the application using a daemon.

### Test stage

Test the deployed application response with a simple request, using curl.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.
It is only a test

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

## Ernie Rebase
Cambio 1
Cambio 2
Cambio 3

## Johnny Márquez Rebase
Commit 1
Commit 2
Commit 3