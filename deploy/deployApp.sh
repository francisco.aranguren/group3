#!/bin/bash

# any future command that fails will exit the script
set -e

## Killing current npm process
for p in $(ps -ef | egrep 'node|npm' | grep -v grep | awk {'print $2'}); 
    do kill -9 $p; 
done

# Prepare workspace
rm -Rf /opt/group3/workspace
mkdir /opt/group3/workspace
mv /tmp/group3.zip /opt/group3/workspace
cd /opt/group3/workspace
unzip group3.zip


#Start the node server
/usr/sbin/daemonize -E BUILD_ID=dontKillMe /opt/group3/startApp.sh

