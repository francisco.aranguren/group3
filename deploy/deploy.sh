#!/bin/bash

# any future command that fails will exit the script
set -e
# Lets write the public key of our aws instance
eval $(ssh-agent -s)
echo "$PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null

# ** Alternative approach
mkdir ~/.ssh
touch ~/.ssh/id_rsa
echo -e "$PRIVATE_KEY" > ~/.ssh/id_rsa
chmod 600 ~/.ssh/id_rsa
touch ~/.ssh/config
echo -e "Host *\n\tStrictHostKeyChecking no\n\n" >> ~/.ssh/config
# ** End of alternative approach

## Connection testing
echo "Connecting to server test..." 
ssh -i ~/.ssh/id_rsa ec2-user@$SERVER 'hostname'

## Send package to EC2 Server
scp -i ~/.ssh/id_rsa group3.zip ec2-user@$SERVER:/tmp/

## Deploy app
echo "Connecting to server..." 
ssh -i ~/.ssh/id_rsa ec2-user@$SERVER 'bash -s' < ./deploy/deployApp.sh
